--PHP CHECKSTYLE
cd ../../Applications/MAMP/htdocs/3A/partiel_final/doc/PHP
php run.php --src ../../../partiel_final --outdir ../Psr --format html,xml --lang fr-fr
php run.php --src ../../../partiel_final --outdir ../Psr --format html,xml --lang fr-fr --exclude ../../doc
php run.php --src ../../../partiel_final --outdir ../Psr --format html,xml --lang fr-fr --exclude /doc --exclude /core/lib


--APIGEN

bash-3.2# php apigen.phar generate --source ../../partiel_final/app --destination apigen --exclude /doc --exclude /core/lib
php apigen.phar generate --source ../../partiel_final/app --destination apigen --exclude /doc --exclude /core/lib
php apigen.phar generate --source ../../partiel_final/app --destination apigen --exclude /doc --exclude /core/lib --tree