//********* Lancer le chrono **************
var chrono_start = new Date();

/**********************************************************
Fonction    : date_fr()
Description : Retourne la date en français
Paramètres  : vide
Retour      : Lundi 12 Janvier 2012 (exemple)
**********************************************************/
function date_fr() 
{
	var ladate = new Date();

	var annee = ladate.getFullYear();

	var tab_jours = new Array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
	var tab_mois = new Array("Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");

	return tab_jours[ladate.getDay()] + " " + ladate.getDate() + " " + tab_mois[ladate.getMonth()] + " " + annee;
}

/**********************************************************
Fonction    : annee()
Description : Retourne l'année AAAA
Paramètres  : vide
Retour      : 2012 (exemple)
**********************************************************/
function annee() 
{
	var ladate = new Date();

	var annee = ladate.getFullYear();

	return annee;
}

/**********************************************************
Fonction    : hh_mm_ss()
Description : Retourne l'heure
Paramètres  : vide
Retour      : 12:45:28 (exemple)
**********************************************************/
function hh_mm_ss() 
{
	var ladate = new Date();
	var heure = ladate.getHours();
	var minute = ladate.getMinutes();
	var seconde = ladate.getSeconds();

	if (heure<10) heure = "0" + heure;
	if (minute<10) minute = "0" + minute;
	if (seconde<10) seconde = "0" + seconde;

	return heure + ":" + minute + ":" + seconde;
}


/**********************************************************
 Fonction    : maPosition()
 Description : permet
 **********************************************************/
function maPosition(position) {
	var infopos = "index.php?";
	infopos += "lat="+position.coords.latitude +"\n";
	infopos += "&lng="+position.coords.longitude+"\n";
	document.getElementById("localisation").href =  infopos;
}

// Fonction de callback en cas d’erreur
function erreurPosition(error) {
	var info = "Erreur lors de la géolocalisation : ";
	switch(error.code) {
		case error.TIMEOUT:
			info += "Timeout !";
			break;
		case error.PERMISSION_DENIED:
			info += "Vous n’avez pas donné la permission";
			break;
		case error.POSITION_UNAVAILABLE:
			info += "La position n’a pu être déterminée";
			break;
		case error.UNKNOWN_ERROR:
			info += "Erreur inconnue";
			break;
	}
	document.getElementById("infoposition").innerHTML = info;
}

if(navigator.geolocation)
	navigator.geolocation.getCurrentPosition(maPosition, erreurPosition, {maximumAge:600000,enableHighAccuracy:true});
