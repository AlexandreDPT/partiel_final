<?php


	//include ("model/model.php");
	include_once("../app/config/param.inc.php");
	include_once("../app/config/connect.bdd.php");
	include("../core/function/function_start_session.php");
	include("../core/function/function_log.php");

	//$debug = array();
	//$retour = array();
	
	$connect = cnx_site_local( 'pdo' );
	my_session_start(NOM_SESSION);

	// DOSSIER
	if(isset($_GET["module"]))
	{
		$module = $_GET["module"];
	}
	else
	{
		$module = "index";
	}
	//PAGE
	if(isset($_GET["action"]))
	{
		$action = $_GET["action"];
	}
	else
	{
		$action = "index";
	}
	
	$url = "../app/controler/".$module."/".$action.".contr.php";
	
	//log_call($url);
			
	if(file_exists($url))
	{		
		include($url);
	}
	else
	{
		include_once("../app/controler/others/404.view.php");
	}
	
	//include_once(BASE_DIR_VALIDE."app/debug/debug.php");

?>