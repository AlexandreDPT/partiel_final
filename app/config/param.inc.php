<?php
/********** **********/
/**
 ** Constantes communes pour page
 **/
	define('NOM_SESSION', "TP0_KHAUV");
	define('MAX_PAGE', 10); // Nombre de posts par page
	define('PREFIX_PAGE', ""); // Pr�fixe pour les pages
	define('LANG', "fr");
	ini_set('allow_url_include', 0);
/**
 ** Constantes pour config Serveur local
 **/
	if (($_SERVER["HTTP_HOST"] == "localhost") or ($_SERVER["HTTP_HOST"] == "127.0.0.1"))
	{
		define('DEBUG', TRUE);
	}
/**
 ** Constantes pour config Serveur en ligne
 **/
	else if ($_SERVER["HTTP_HOST"] == "ns366377.ovh.net")
	{
		define('DEBUG', FALSE);
	}
	
	define('BASE_DIR_VALIDE',"http://".$_SERVER["HTTP_HOST"].dirname($_SERVER['SCRIPT_NAME']));

?>