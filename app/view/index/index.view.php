

<?php include("../app/include/header.inc.php");?>


	<div id="page" class="row">
		<div class="row">
			<p><a href="https://bitbucket.org/AlexandreDPT/partiel_final">Lien Git</a></p>
		</div>
		<div class="row">

			<h2>En ce moment.. </h2>
			<div class="col-md-4">
				<h2>
					<img src="<?php echo $data['current_condition']['icon_big'] ?>" class="center" alt="">
					<?php echo $data['current_condition']['tmp']; ?> C°
				</h2>
			</div>
			<div class="col-md-4">
				<h3 class="text-uppercase"><?php echo $data['current_condition']['condition'] ?></h3>
				<table class="table table-striped">
					<tbody>
					<tr>
						<th>Pression</th>
						<td><?php echo $data['current_condition']['pressure'] ?> hPa</td>
					</tr>
					<tr>
						<th>Humidité</th>
						<td><?php echo $data['current_condition']['humidity'] ?> %</td>
					</tr>
					<tr>
						<th>Humidité</th>
						<td><?php echo $data['current_condition']['humidity'] ?></td>
					</tr>
					<tr>
						<th>Vents (rafales)</th>
						<td><?php echo $data['current_condition']['wnd_spd'] ?> (<?php echo $data['current_condition']['wnd_gust'] ?>) km/h</td>
					</tr>

					</tbody>
				</table>
			</div>
			<div class="col-md-4">
				<h3>Lieu <a id="localisation" href="">Me localiser</a> </h3>
				<table class="table table-striped">
					<tbody>
					<tr>
						<th>Latitude</th>
						<td><?php echo $data['city_info']['latitude'] ?></td>
					</tr>
					<tr>
						<th>Longitude</th>
						<td><?php echo $data['city_info']['longitude'] ?></td>
					</tr>
					<tr>
						<th>Levé du soleil</th>
						<td><?php echo $data['city_info']['sunrise'] ?> h</td>
					</tr>
					<tr>
						<th>se couchant</th>
						<td><?php echo $data['city_info']['sunset'] ?> h</td>
					</tr>

					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div id="map-canvas"></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-uppercase">Prévision</h2>
				<div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" class="btn day btn-default" id="day_0"><?php echo $data['fcst_day_0']['day_long'] ?></button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn day btn-default" id="day_1"><?php echo $data['fcst_day_1']['day_long'] ?></button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn day btn-default" id="day_2"><?php echo $data['fcst_day_2']['day_long'] ?></button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn day btn-default" id="day_3"><?php echo $data['fcst_day_3']['day_long'] ?></button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn day btn-default" id="day_4"><?php echo $data['fcst_day_4']['day_long'] ?></button>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<h2>
					<img id="previmg" src="<?php echo $data['fcst_day_0']['icon_big'] ?>" class="center" alt=""><br/>
					Température max : <span id="prevmin"><?php echo $data['fcst_day_0']['tmin']; ?></span> C°</br>
					Température min : <span id="prevmax"><?php echo $data['fcst_day_0']['tmax']; ?></span> C°
				</h2>
			</div>
			<div class="col-lg-6" style="height: 200px; overflow: scroll;">
				<table class="table table-striped">
					<tbody>
				<?php
				$i = 0;
				while ($i < 24) {

					echo ("<tr>
						<th>".$i."H00</th>
						<td><img src='".$data['fcst_day_0']['hourly_data'][$i.'H00']['ICON']."'/>
						".$data['fcst_day_0']['hourly_data'][$i.'H00']['CONDITION']."
						".$data['fcst_day_0']['hourly_data'][$i.'H00']['TMP2m']."C° </td>
					</tr>");
					$i++;
				}
				?>
					</tbody>
				</table>
			</div>

		</div>

		<div class="row">

		</div>
	</div>

<?php include("../app/include/footer.inc.php"); ?>

<script type="text/javascript" src="public/js/evt.js"></script>

</body>
</html>
